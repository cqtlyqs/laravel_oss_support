# laravel_oss_support

#### 介绍
集成阿里云的OSS
集成常用接口

#### 使用说明

1. 引入
```
composer require ktnw/oss_support
```
2. 发布
```
php artisan vendor:publish --provider="Ktnw\OssSupport\Providers\OssSupportServiceProvider"
```

3.修改相关class的namespace
```
php artisan support:oss
```
4.示例参考OssController
