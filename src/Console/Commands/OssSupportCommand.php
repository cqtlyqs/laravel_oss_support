<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class OssSupportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'support:oss';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update oss class namespace.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'support oss';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->namespaceUpdate();
        $this->info($this->type . ' created successfully.');
    }



    /**
     * 修改相关类的namespace
     */
    private function namespaceUpdate()
    {
        $classes = [
            ["classPath" => 'Http\Controllers\Api\OssController.php', 'namespace' => 'App\Http\Controllers\Api'],
        ];

        foreach ($classes as $item) {
            $path = app_path($item['classPath']);
            if (!file_exists($path)) {
                $this->info("namespace update fail: file[$path] not exist.");
                return;
            }
            $content = File::get($path);
            File::put($path, preg_replace("/namespace.*/", "namespace " . $item['namespace'] . ";", $content));
        }
    }




}
