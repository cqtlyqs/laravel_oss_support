<?php

namespace Ktnw\VodSupport\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Ktnw\OssSupport\Utils\OssUtils;
use ReflectionClass;
use ReflectionException;

/**
 * 阿里云视频点播相关接口
 *
 */
class OssController extends Controller
{

    /**
     * 操作成功标志
     */
    private const SUCCESS = 1;

    /**
     * 操作失败标志
     */
    private const FAIL = -1;

    /**
     * 响应代码的key
     */
    private const CODE = "code";

    /**
     * 响应提示信息的key
     */
    private const MESSAGE = "message";

    /**
     * 响应代码的key
     */
    private const DATA = "data";

    /**
     * web post object to oss signature
     * 前端使用postObject上传文件到OSS的服务器端签名
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function webPostToOssSignature(Request $request): JsonResponse
    {
        // 上传类型 1-上传到公用目录(可公开访问);2-上传到私有目录(需授权签名后访问);
        $upType    = $request->input("upType", 2);
        $catalogue = $request->input("catalogue", "");
        $response  = OssUtils::ossSignature($upType, $catalogue);
        return $this->success($response);
    }


    /**
     * 利用反射封装前端传递参数
     * @param object $class
     * @return mixed
     */
    private function getParameters(&$class)
    {
        try {
            $refClass   = new ReflectionClass($class);
            $properties = $refClass->getProperties();
            // 反射获取到类的属性信息
            foreach ($properties as $property) {
                $prop = $property->getName();
                $val  = request($prop, '');
                if (isset($val) && $val === "") {
                    continue;
                }
                // 替换特殊字符
                if (is_string($val)) {
                    $val = $this->strReplaceHtml($val);
                }
                if (!$property->isPublic()) {
                    // 给非公有属性设置访问权限
                    $property->setAccessible(true);
                }
                $property->setValue($class, $val);
            }
        } catch (ReflectionException $e) {
            Log::error($e->getMessage());
        }
    }

    /**
     * 替换字符串中的特殊字符
     * \r \n \r\n 替换为 <br>
     * &转成&amp;
     * "转成&quot;
     * ' 转成&#039;
     * <转成&lt;
     * >转成&gt;
     * @param $value
     * @return string|string[]|null
     */
    static function strReplaceHtml($value)
    {
        if ($value === '0' || $value === 0) {
            return '0';
        }

        if (empty($value)) {
            return '';
        }
        $value = preg_replace('/\r\n|\r|\n/', '<br>', $value);
        $value = htmlspecialchars(nl2br($value));
        return preg_replace("/'/", '&#039;', $value);
    }


    /**
     * 操作成功响应
     * @param mixed $data
     * @param string $message 提示信息
     * @return JsonResponse
     */
    private function success($data = [], string $message = ''): JsonResponse
    {
        return $this->out([self::CODE => self::SUCCESS, self::MESSAGE => $message, self::DATA => $data]);
    }

    /**
     * 操作失败响应
     * @param string $message
     * @return JsonResponse
     */
    private function fail(string $message = ''): JsonResponse
    {
        return $this->out([self::CODE => self::FAIL, self::MESSAGE => $message]);
    }

    /**
     * 响应
     * @param $data
     * @return mixed
     */
    private function out($data): JsonResponse
    {
        return response()->json($data)
            ->header('Content-Type', 'text/json')
            ->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }

}
