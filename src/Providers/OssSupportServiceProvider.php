<?php

namespace Ktnw\OssSupport\Providers;

use Illuminate\Support\ServiceProvider;

class OssSupportServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     *
     * @return void
     */
    public function boot()
    {
        // 拷贝文件
        $this->publishes($this->getPublishFiles());
    }

    private function getPublishFiles(): array
    {
        $data       = $this->publishData();
        $srcData    = array_column($data, "src");
        $targetData = array_column($data, "target");
        $publishes  = [];
        for ($i = 0; $i < count($srcData); $i++) {
            $publishes[$srcData[$i]] = $targetData[$i];
        }
        return $publishes;
    }

    private function publishData(): array
    {
        return [
            ['src' => __DIR__ . '/../../resources/config/ossConfig.php', 'target' => config_path('ossConfig.php')],
            ['src' => __DIR__ . '/../Controllers/OssController.php', 'target' => app_path("Http/Controllers/Api/OssController.php")],
            ['src' => __DIR__ . '/../Console/Commands/OssSupportCommand.php', 'target' => app_path("Console/Commands/OssSupportCommand.php")],
        ];
    }

}