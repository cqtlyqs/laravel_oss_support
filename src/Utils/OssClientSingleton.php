<?php
/**
 * 单例创建ossClient对象
 * Created by PhpStorm.
 * User: youqingsong
 * Date: 2022/02/17
 * Time: 15:55
 */

namespace Ktnw\OssSupport\Utils;


use OSS\Core\OssException;
use OSS\OssClient;

class OssClientSingleton
{
    // 创建静态私有的变量保存该类对象
    static private $ossClient;

    // 锁构造器 防止使用new直接创建对象
    private function __construct()
    {
    }

    // 防止使用clone克隆对象
    private function __clone()
    {
    }

    /**
     * @throws OssException
     */
    static public function getInstance(): OssClient
    {
        //判断$instance是否是Singleton的对象，不是则创建
        if (!self::$ossClient instanceof OssClient) {
            $access_key_id     = config("ossConfig.oss_access_key_id");
            $access_key_secret = config("ossConfig.oss_access_key_secret");
            $endpoint          = config("ossConfig.oss_endpoint");
            self::$ossClient   = new OssClient($access_key_id, $access_key_secret, $endpoint);
        }
        return self::$ossClient;
    }
}