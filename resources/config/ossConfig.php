<?php
/**
 * 阿里云OSS相关的配置
 */
return [
    'oss_access_key_id'        => '',
    'oss_access_key_secret'    => '',
    'oss_bucket_name'          => '',
    'oss_access_url'           => '',
    'oss_endpoint'             => "",
    'oss_access_validity_time' => 2 * 60,   // oss访问的有效期 单位：秒
    'pub_access_catalog'       => "image/", // oss公开访问的目录
    'max_size'                 => 20,       // oss可上传的最大文件(单位: M)
];